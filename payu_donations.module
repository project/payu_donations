<?php

/**
 * @file
 * Primary module hooks for PayU Donations module.
 */

declare(strict_types=1);

use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\filter\FilterProcessResult;

/**
 * Implements hook_help().
 *
 * Add help page for module.
 */
function payu_donations_help(string $route_name, RouteMatchInterface $route_match): string|FilterProcessResult {
  switch ($route_name) {
    case 'help.page.payu_donations':
      $text = (bool) file_get_contents(__DIR__ . '/README.md') ? (string) file_get_contents(__DIR__ . '/README.md') : '';

      if (!\Drupal::moduleHandler()->moduleExists('markdown')) {
        return '<pre>' . Html::escape($text) . '</pre>';
      }

      // Use the Markdown filter to render the README.
      $filter_manager = \Drupal::service('plugin.manager.filter');
      $settings = \Drupal::configFactory()->get('markdown.settings')->getRawData();
      $config = ['settings' => $settings];
      /** @var \Drupal\filter\Plugin\FilterBase */
      $filter = $filter_manager->createInstance('markdown', $config);

      return $filter->process($text, 'en');
  }

  return '';
}

/**
 * Implements hook_theme().
 *
 * Add module themes.
 */
function payu_donations_theme(): array {
  return [
    'payu_donations_block' => [
      'variables' => [
        'form' => [],
      ],
    ],
  ];
}

/**
 * Implements hook_entity_operation().
 */
function payu_donations_entity_operation(EntityInterface $entity): array {
  $operations = [];
  if ($entity->getEntityTypeId() == 'payu_donations_payment') {
    $operations['view'] = [
      'title' => t('View'),
      'url' => Url::fromRoute('entity.payu_donations_payment.canonical', ['payu_donations_payment' => $entity->id()]),
      'weight' => 50,
    ];
  }
  return $operations;
}
