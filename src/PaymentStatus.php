<?php

declare(strict_types=1);

namespace Drupal\payu_donations;

/**
 * PayUDonationsPayments statuses.
 */
enum PaymentStatus: string {
  case NEW = 'NEW';
  case PENDING = 'PENDING';
  case CANCELED = 'CANCELED';
  case REJECTED = 'REJECTED';
  case COMPLETED = 'COMPLETED';
  case WAITING_FOR_CONFIRMATION = 'WAITING_FOR_CONFIRMATION';
}
