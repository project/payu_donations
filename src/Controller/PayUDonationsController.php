<?php

declare(strict_types=1);

namespace Drupal\payu_donations\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\payu_donations\PaymentStatus;
use Drupal\payu_donations\PayUDonationsPaymentInterface;
use Drupal\payu_donations\Utils;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Returns responses for PayU Donations routes.
 */
class PayUDonationsController extends ControllerBase {

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config manager.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $payuLogger
   *   The Payu logger.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    ConfigFactoryInterface $config_factory,
    protected LoggerChannelInterface $payuLogger,
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('config.factory'),
      $container->get('logger.channel.payu_donations')
    );
  }

  /**
   * Receive request from payu and stores actual status of payment.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Response.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \OpenPayU_Exception
   * @throws \OpenPayU_Exception_Configuration
   */
  public function receiveNotification(Request $request): Response {
    $response = json_decode($request->getContent());

    /* @phpstan-ignore-next-line */
    if (!isset($response->order)) {
      throw new \OpenPayU_Exception('Order not found');
    }

    $order = $response->order ?? NULL;
    $order_id = $order->orderId ?? NULL;

    if (empty($order_id)) {
      throw new \OpenPayU_Exception('Order id not found');
    }

    $payment = $this->entityTypeManager
      ->getStorage('payu_donations_payment')
      ->loadByProperties(['remote_id' => $order_id]);
    $payment = reset($payment);

    if (!$payment instanceof PayUDonationsPaymentInterface) {
      throw new \OpenPayU_Exception('Payment is not an instance of PayUDonationsPaymentInterface');
    }

    /** @var \Drupal\Core\Config\ImmutableConfig */
    $config_pauy = $this->configFactory->get('payu_donations.settings');
    $options = Utils::getPayuConfiguration($config_pauy);
    /** @var string */
    $signature_key = $config_pauy->get('payu_signature_key');

    Utils::configurePayU(
      $options['payu_environment'],
      $options['payu_pos_id'],
      $options['payu_second_key_md5'],
      $options['payu_client_id'],
      $options['payu_client_secret']
    );

    if ($options['payu_environment'] !== 'sandbox') {
      if (!$this->hasValidSignature($request, $signature_key)) {
        \OpenPayU_Order::cancel($order_id);
        $this->payuLogger->notice('The order with id: @order_id, has been canceled.', [
          '@order_id' => $order_id,
        ]);

        return new Response('Notification has been cancelled');
      }
    }

    $payment_status = PaymentStatus::tryFrom(strval($order->status));
    $payment->setStatus($payment_status);
    $payment->save();

    return new Response('Notification OK');
  }

  /**
   * Validates request signature using drupal commerce signature.
   *
   * @param \Symfony\Component\HttpFoundation\Request $on_notify_request
   *   The Request object.
   * @param string $signature_key
   *   Signature key.
   *
   * @return bool
   *   Returns true if succeeds, false of not.
   */
  public function hasValidSignature(Request $on_notify_request, string $signature_key = "Drupal"): bool {
    /** @var string */
    $header_raw = $on_notify_request->headers->get('Openpayu-Signature');
    /** @var array */
    $header = \OpenPayU_Util::parseSignature($header_raw);
    $request_signature = $header['signature'];
    $request_hash_method = $header['algorithm'];
    $request_content = $on_notify_request->getContent();

    return \OpenPayU_Util::verifySignature($request_content, $request_signature, $signature_key, $request_hash_method);
  }

}
