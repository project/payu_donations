<?php

declare(strict_types=1);

namespace Drupal\payu_donations\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Defines a route controller for handling the PayU response.
 */
class PayURedirectHandler extends ControllerBase {

  /**
   * Handle PayU redirect base on order status.
   */
  public function handleRedirect(Request $request): RedirectResponse {
    $session = $request->getSession();
    $from_payu = $session->get('from_payu');

    if (!(bool) $from_payu) {
      return new RedirectResponse('/');
    }

    /** @var string */
    $succeed_url = $session->get('succeed_url');
    /** @var string */
    $canceled_url = $session->get('canceled_url');

    $session->remove('succeed_url');
    $session->remove('canceled_url');
    $session->remove('from_payu');

    $has_error = FALSE;

    if (!empty($request->query->get('error')) && $request->query->get('error') === '501') {
      $has_error = TRUE;
    }

    if ($has_error && !empty($canceled_url)) {
      return new RedirectResponse($canceled_url);
    }

    if (!$has_error && !empty($succeed_url)) {
      return new RedirectResponse($succeed_url);
    }

    return new RedirectResponse('/');
  }

}
