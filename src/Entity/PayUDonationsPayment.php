<?php

declare(strict_types=1);

namespace Drupal\payu_donations\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\payu_donations\PaymentStatus;
use Drupal\payu_donations\PayUDonationsPaymentInterface;

/**
 * Defines the payu donations payment entity class.
 *
 * @ContentEntityType(
 *   id = "payu_donations_payment",
 *   label = @Translation("Payu Donations Payment"),
 *   label_collection = @Translation("Payu Donations Payments"),
 *   label_singular = @Translation("payu donations payment"),
 *   label_plural = @Translation("payu donations payments"),
 *   label_count = @PluralTranslation(
 *     singular = "@count payu donations payments",
 *     plural = "@count payu donations payments",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\payu_donations\PayUDonationsPaymentListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "form" = {
 *       "default" = "Drupal\Core\Entity\ContentEntityForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "payu_donations_payment",
 *   admin_permission = "administer payu donations payment",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "id",
 *   },
 *   links = {
 *     "collection" = "/admin/config/payu-donations/payment",
 *     "canonical" = "/admin/config/payu-donations/payment/{payu_donations_payment}",
 *   },
 * )
 */
class PayUDonationsPayment extends ContentEntityBase implements PayUDonationsPaymentInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['description'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Description'))
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['remote_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Remote ID'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['remote_state'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Remote State'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['amount'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Amount'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['currency'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Currency'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Status'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the payu donations payment was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the payu donations payment was last edited.'));

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription(): string {
    return $this->get('description')->getString();
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteId(): string {
    return $this->get('remote_id')->getString();
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteState(): string {
    return $this->get('remote_state')->getString();
  }

  /**
   * {@inheritdoc}
   */
  public function getAmount(): string {
    return $this->get('amount')->getString();
  }

  /**
   * {@inheritdoc}
   */
  public function getCurrencyCode(): string {
    return $this->get('currency')->getString();
  }

  /**
   * {@inheritdoc}
   */
  public function setStatus(?PaymentStatus $status): PayUDonationsPaymentInterface {
    $this->set('status', $status === NULL ? NULL : $status->value);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getStatus(): ?PaymentStatus {
    return PaymentStatus::tryFrom($this->get('status')->getString());
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime(): int {
    return intval($this->get('created')->getString());
  }

  /**
   * {@inheritdoc}
   */
  public function getChangedTime(): int {
    return intval($this->get('changed')->getString());
  }

}
