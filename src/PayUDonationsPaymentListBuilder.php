<?php

declare(strict_types=1);

namespace Drupal\payu_donations;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a list controller for the payu donations payment entity type.
 */
class PayUDonationsPaymentListBuilder extends EntityListBuilder {

  /**
   * The date formatter service.
   */
  protected DateFormatterInterface $dateFormatter;

  /**
   * Constructs a new PayUDonationsPaymentListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   */
  public function __construct(
      EntityTypeInterface $entity_type,
      EntityStorageInterface $storage,
      DateFormatterInterface $date_formatter,
  ) {
    parent::__construct($entity_type, $storage);
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = [];
    $build['table'] = parent::render();
    $total = $this->getStorage()
      ->getQuery()
      ->accessCheck(FALSE)
      ->count()
      ->execute();
    $build['summary']['#markup'] = $this->t('Total payu donations payments: @total', ['@total' => $total]);

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [];
    $header['id'] = $this->t('ID');
    $header['remote_id'] = $this->t('Order Id');
    $header['description'] = $this->t('Description');
    $header['amount'] = $this->t('Amount');
    $header['currency'] = $this->t('Currency');
    $header['remote_state'] = $this->t('Remote state');
    $header['created'] = $this->t('Created');
    $header['changed'] = $this->t('Updated');
    $header['status'] = $this->t('Status');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\payu_donations\PayUDonationsPaymentInterface $entity */
    $row = [];
    $row['id'] = $entity->id();
    $row['remote_id'] = strtolower($entity->getRemoteId());
    $row['description'] = $entity->getDescription();
    $row['amount'] = $entity->getAmount();
    $row['currency'] = $entity->getCurrencyCode();
    $row['remote_state'] = $entity->getRemoteState();
    $row['created'] = $this->dateFormatter->format($entity->getCreatedTime());
    $row['changed'] = $this->dateFormatter->format($entity->getChangedTime());
    $row['status'] = $entity->getStatus()?->value;

    return $row + parent::buildRow($entity);
  }

}
