<?php

declare(strict_types=1);

namespace Drupal\payu_donations;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a payu donations payment entity type.
 */
interface PayUDonationsPaymentInterface extends ContentEntityInterface, EntityChangedInterface {

  /**
   * Get the PayUDonationsPayment description.
   */
  public function getDescription(): string;

  /**
   * Get the PayUDonationsPayment remote id.
   */
  public function getRemoteId(): string;

  /**
   * Get the PayUDonationsPayment remote state.
   */
  public function getRemoteState(): string;

  /**
   * Get the PayUDonationsPayment amount.
   */
  public function getAmount(): string;

  /**
   * Get the PayUDonationsPayment currency code.
   */
  public function getCurrencyCode(): string;

  /**
   * Set the PayUDonationsPayment status.
   */
  public function setStatus(?PaymentStatus $status): PayUDonationsPaymentInterface;

  /**
   * Get the PayUDonationsPayment status.
   */
  public function getStatus(): ?PaymentStatus;

  /**
   * Get the PayUDonationsPayment created time.
   */
  public function getCreatedTime(): int;

  /**
   * Get the PayUDonationsPayment changed time.
   */
  public function getChangedTime(): int;

}
