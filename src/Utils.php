<?php

declare(strict_types=1);

namespace Drupal\payu_donations;

use CommerceGuys\Intl\Currency\CurrencyRepository;
use Drupal\Core\Config\Config;

/**
 * Provides utilities for payu_dontations module.
 */
class Utils {

  /**
   * Configures payU.
   */
  public static function configurePayU(
    string $payu_environment,
    string $payu_pos_id,
    string $payu_second_key_md5,
    string $payu_client_id,
    string $payu_client_secret,
  ): void {
    \OpenPayU_Configuration::setEnvironment($payu_environment);
    \OpenPayU_Configuration::setMerchantPosId($payu_pos_id);
    \OpenPayU_Configuration::setSignatureKey($payu_second_key_md5);
    \OpenPayU_Configuration::setOauthClientId($payu_client_id);
    \OpenPayU_Configuration::setOauthClientSecret($payu_client_secret);
  }

  /**
   * Returns payU configuration options.
   *
   * @param \Drupal\Core\Config\Config $config
   *   PayU configuration object.
   * @param array $block_configuration
   *   Block id.
   *
   * @return array
   *   Array with config options.
   */
  public static function getPayuConfiguration(Config $config, ?array $block_configuration = NULL): array {
    $config_fields = [
      'payu_environment',
      'payu_pos_id',
      'payu_client_id',
      'payu_client_secret',
      'payu_second_key_md5',
      'payu_currency',
      'payu_payment_description',
      'payu_succeed_url_redirect',
      'payu_canceled_url_redirect',
    ];
    $payu_configuration = [];

    foreach ($config_fields as $field_id) {
      if (isset($block_configuration[$field_id]) && !empty($block_configuration[$field_id])) {
        $payu_configuration[$field_id] = $block_configuration[$field_id];
        continue;
      }

      $payu_configuration[$field_id] = $config->get($field_id);
    }

    return $payu_configuration;
  }

  /**
   * Returns payU configuration options.
   *
   * @return array
   *   Array with currencies.
   */
  public static function getPayuCurrencies(): array {
    $currency_repository = new CurrencyRepository();
    return array_keys($currency_repository->getList());
  }

}
