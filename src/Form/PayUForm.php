<?php

declare(strict_types=1);

namespace Drupal\payu_donations\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Drupal\payu_donations\Entity\PayUDonationsPayment;
use Drupal\payu_donations\Utils;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Provides a PayU Donations form.
 */
class PayUForm extends FormBase {

  /**
   * The logger service.
   */
  protected LoggerChannelInterface $logger;

  /**
   * The current user account.
   */
  protected AccountProxyInterface $account;

  /**
   * The session service.
   */
  protected Session $session;

  /**
   * Success status.
   */
  private const SUCCESS_STATUS = 'SUCCESS';

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): PayUForm {
    $instance = parent::create($container);
    $instance->logger = $container->get('logger.channel.payu_donations');
    $instance->account = $container->get('current_user');
    $instance->session = $container->get('session');
    $instance->configFactory = $container->get('config.factory');
    $instance->messenger = $container->get('messenger');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'payu_donations_form';
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param string $submit_button_text
   *   Text displayed on the form approval button.
   * @param string $block_id
   *   Block id.
   */
  public function buildForm(
    array $form,
    FormStateInterface $form_state,
    string $submit_button_text = '',
    string $block_id = '',
  ): array {

    $form['payu_amount'] = [
      '#type' => 'number',
      '#min' => 0.01,
      '#step' => 0.01,
      '#title' => $this->t('Amount'),
      '#required' => TRUE,
      '#default_value' => $form_state->getValues()['payu_amount'] ?? NULL,
    ];

    if (!empty($form_state->getUserInput())) {
      $block_id = $form_state->getUserInput()['block_id'] ?? NULL;
    }

    $form['block_id'] = [
      '#type' => 'hidden',
      '#default_value' => $block_id,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $submit_button_text,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    /** @var string|null */
    $payu_amount = $form_state->getValue('payu_amount');

    if (!empty($payu_amount) && !(bool) preg_match('/^\d+\.{0,1}\d*$/', $payu_amount)) {
      $form_state->setErrorByName('payu_amount', $this->t('The amount can only contain numbers!'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $configuration = $this->configFactory->get('payu_donations.settings');
    /** @var string */
    $block_id = $form_state->getValue('block_id');
    /** @var array */
    $block_configuration = $this->configFactory->get('block.block.' . $block_id)->get('settings');
    $options = Utils::getPayuConfiguration($configuration, $block_configuration);

    Utils::configurePayU(
      $options['payu_environment'],
      $options['payu_pos_id'],
      $options['payu_second_key_md5'],
      $options['payu_client_id'],
      $options['payu_client_secret']
    );

    $paymentData = [];

    $order = $this->createOrder($form_state, $options);

    try {
      /** @var \OpenPayU_Result */
      $response = \OpenPayU_Order::create($order);

      $payuOrder = $response->getResponse();
      /* @phpstan-ignore-next-line */
      $payuOrderExtId = $payuOrder->orderId;
      $status_desc = \OpenPayU_Util::statusDesc($response->getStatus());

      $this->session->set('succeed_url', $options['payu_succeed_url_redirect']);
      $this->session->set('canceled_url', $options['payu_canceled_url_redirect']);
      $this->session->set('from_payu', TRUE);

      $paymentData['remote_id'] = $payuOrderExtId;
      $paymentData['currency'] = $order['currencyCode'];
      $paymentData['amount'] = $form_state->getValue('payu_amount');
      $paymentData['remote_state'] = $status_desc;
      $paymentData['description'] = $order['description'];
      $paymentData['status'] = 'PENDING';

      $payment = PayUDonationsPayment::create($paymentData);
      $payment->save();

      if ($response->getStatus() === self::SUCCESS_STATUS) {
        $response_obj = $response->getResponse();
        /* @phpstan-ignore-next-line */
        $redirect_uri = $response_obj->redirectUri;

        if (!isset($redirect_uri) || !is_string($redirect_uri)) {
          $context = [
            '@error' => 'Redirect URI is missing',
            '@user_id' => $this->account->id(),
          ];

          $this->messenger()->addError($this->t('Error: @error | User Id: @user_id', $context));
          $this->logger->error('Error: @error | User Id: @user_id', $context);

          return;
        }

        $response_uri = new TrustedRedirectResponse($redirect_uri);
        $form_state->setResponse($response_uri);
      }
      else {
        $this->messenger->addError(
          $this->t('Status: @status: @status_desc | User Id: @user_id', [
            '@status' => $response->getStatus(),
            '@status_desc' => $status_desc,
            '@user_id' => $this->account->id(),
          ])
        );
        $this->logger->error('Status: @status: @status_desc | User Id: @user_id', [
          '@status' => $response->getStatus(),
          '@status_desc' => $status_desc,
          '@user_id' => $this->account->id(),
        ]);
      }
    }
    catch (\OpenPayU_Exception $e) {
      $this->messenger->addError($e->getMessage());
      $this->logger->error(
        'Error: @errorMessage | User Id: @userId',
        ['@errorMessage' => $e->getMessage(), '@userId' => $this->account->id()],
      );
    }
  }

  /**
   * Creates an array with order data.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param array $options
   *   Options.
   *
   * @return array
   *   Array with order data.
   */
  protected function createOrder(FormStateInterface $form_state, array $options): array {
    $products = [
      [
        'name' => $this->t('Donation'),
        'unitPrice' => $form_state->getValue('payu_amount'),
        'quantity' => 1,
      ],
    ];
    $order = [];
    $order['description'] = $options['payu_payment_description'];
    $order['customerIp'] = $this->getRequest()->getClientIp();
    $order['merchantPosId'] = (bool) \OpenPayU_Configuration::getOauthClientId() ? \OpenPayU_Configuration::getOauthClientId() : \OpenPayU_Configuration::getMerchantPosId();
    $order['currencyCode'] = $options['payu_currency'];
    $order['totalAmount'] = $form_state->getValue('payu_amount');
    $order['continueUrl'] = Url::fromRoute('payu_donations.redirect_handler', [], ['absolute' => TRUE])->toString();
    $order['notifyUrl'] = Url::fromRoute('payu_donations.notify', [], ['absolute' => TRUE])->toString();
    $order['products'] = $products;
    $order['totalAmount'] *= 100;

    return $order;
  }

}
