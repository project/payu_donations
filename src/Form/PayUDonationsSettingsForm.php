<?php

declare(strict_types=1);

namespace Drupal\payu_donations\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\payu_donations\Utils;

/**
 * Configure PayU settings for this site.
 */
class PayUDonationsSettingsForm extends ConfigFormBase {

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'payu_donations_settings';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form = array_merge($form, $this->getFormFields());

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  private function getFormFields(): array {
    $configuration = $this->config('payu_donations.settings');

    $form = [];
    $form['settings_description'] = [
      '#type' => 'html_tag',
      '#tag' => 'span',
      '#value' => $this->t('Here you can find the global module settings used by the blocks. Each block can individually overwrite these settings.'),
    ];

    $form['payu_environment'] = [
      '#type' => 'select',
      '#title' => $this->t('Environment mode'),
      '#options' => [
        'secure' => $this->t('Production'),
        'sandbox' => $this->t('Sandbox'),
      ],
      '#required' => TRUE,
      '#default_value' => $configuration->get('payu_environment') ?? "",
    ];

    $form['payu_pos_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Point of sale ID (pos_id)'),
      '#required' => TRUE,
      '#default_value' => $configuration->get('payu_pos_id') ?? "",
    ];

    $form['payu_second_key_md5'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Second key (MD5)'),
      '#required' => TRUE,
      '#default_value' => $configuration->get('payu_second_key_md5') ?? "",
    ];

    $form['payu_client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OAuth protocol - client_id'),
      '#required' => TRUE,
      '#default_value' => $configuration->get('payu_client_id') ?? "",
    ];

    $form['payu_client_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OAuth protocol - client_secret'),
      '#required' => TRUE,
      '#default_value' => $configuration->get('payu_client_secret') ?? "",
    ];

    $form['payu_signature_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Signature key'),
      '#required' => TRUE,
      '#default_value' => $configuration->get('payu_signature_key') ?? "Drupal",
    ];

    $form['payu_currency'] = [
      '#type' => 'select',
      '#options' => array_combine(Utils::getPayuCurrencies(), Utils::getPayuCurrencies()),
      '#title' => $this->t('Currency (ISO-4217)'),
      '#required' => TRUE,
      '#default_value' => $configuration->get('payu_currency') ?? '',
    ];

    $form['payu_payment_description'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Payment description'),
      '#required' => TRUE,
      '#default_value' => $configuration->get('payu_payment_description') ?? '',
    ];

    $form['payu_submit_button_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Text displayed on the form approval button'),
      '#required' => TRUE,
      '#default_value' => $configuration->get('payu_submit_button_text') ?? '',
    ];

    $form['payu_succeed_url_redirect'] = [
      '#type' => 'url',
      '#title' => $this->t('URL to redirect after succeed payment'),
      '#default_value' => $configuration->get('payu_succeed_url_redirect') ?? '',
    ];

    $form['payu_canceled_url_redirect'] = [
      '#type' => 'url',
      '#title' => $this->t('URL to redirect after canceled payment'),
      '#default_value' => $configuration->get('payu_canceled_url_redirect') ?? '',
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $config = $this->config('payu_donations.settings');

    foreach (array_keys($this->getFormFields()) as $field) {
      $config->set($field, $form_state->getValue($field));
    }

    $config->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  protected function getEditableConfigNames() {
    return ['payu_donations.settings'];
  }

}
