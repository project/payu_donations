<?php

declare(strict_types=1);

namespace Drupal\payu_donations\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\payu_donations\Utils;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a PayU block.
 *
 * @Block(
 *   id = "payu_donations_block",
 *   admin_label = @Translation("PayU Block"),
 *   category = @Translation("Payment")
 * )
 */
class PayUBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Constructs a PayUBlock object.
   *
   * @param array $configuration
   *   Configuration.
   * @param string $plugin_id
   *   Plugin id.
   * @param array $plugin_definition
   *   Plugin definition.
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   Config factory.
   * @param \Drupal\Core\Form\FormBuilderInterface $formBuilder
   *   FormBuilder interface.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $payuLogger
   *   The Payu logger.
   */
  public function __construct(
    array $configuration,
    string $plugin_id,
    array $plugin_definition,
    protected ConfigFactory $configFactory,
    protected FormBuilderInterface $formBuilder,
    protected LoggerChannelInterface $payuLogger,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
      ContainerInterface $container,
      array $configuration,
      $plugin_id,
      $plugin_definition,
  ): static {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('form_builder'),
      $container->get('logger.channel.payu_donations')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm(mixed $form, FormStateInterface $form_state): array {
    $form = parent::blockForm($form, $form_state);
    $configuration = $this->getConfiguration();
    $link = Url::fromRoute('payu_donations.settings_form')->toString();

    $form['settings_description'] = [
      '#type' => 'html_tag',
      '#tag' => 'span',
      '#value' => $this->t('Block settings are provided by <a target="_blank" href="@link_settings">global module settings</a>. If any of the fields are filled, the global setting will be overwritten.', ['@link_settings' => $link]),
    ];

    $form['payu_environment'] = [
      '#type' => 'select',
      '#title' => $this->t('Environment mode'),
      '#options' => [
        'secure' => $this->t('Production'),
        'sandbox' => $this->t('Sandbox'),
      ],
      '#default_value' => $configuration['payu_environment'] ?? '',
    ];

    $form['payu_pos_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Point of sale ID (pos_id)'),
      '#default_value' => $configuration['payu_pos_id'] ?? '',
    ];

    $form['payu_second_key_md5'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Second key (MD5)'),
      '#default_value' => $configuration['payu_second_key_md5'] ?? '',
    ];

    $form['payu_client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OAuth protocol - client_id'),
      '#default_value' => $configuration['payu_client_id'] ?? '',
    ];

    $form['payu_client_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OAuth protocol - client_secret'),
      '#default_value' => $configuration['payu_client_secret'] ?? '',
    ];

    $form['payu_signature_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Signature key'),
      '#default_value' => $configuration['payu_signature_key'] ?? '',
    ];

    $form['payu_currency'] = [
      '#type' => 'select',
      '#options' => array_combine(Utils::getPayuCurrencies(), Utils::getPayuCurrencies()),
      '#title' => $this->t('Currency (ISO-4217)'),
      '#default_value' => $configuration['payu_currency'] ?? '',
    ];

    $form['payu_payment_description'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Payment description'),
      '#default_value' => $configuration['payu_payment_description'] ?? '',
    ];

    $form['payu_submit_button_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Text displayed on the form approval button'),
      '#default_value' => $configuration['payu_submit_button_text'] ?? '',
    ];

    $form['payu_succeed_url_redirect'] = [
      '#type' => 'url',
      '#title' => $this->t('URL to redirect after succeed payment'),
      '#default_value' => $configuration['payu_succeed_url_redirect'] ?? '',
    ];

    $form['payu_canceled_url_redirect'] = [
      '#type' => 'url',
      '#title' => $this->t('URL to redirect after canceled payment'),
      '#default_value' => $configuration['payu_canceled_url_redirect'] ?? '',
    ];

    $form['payu_options'] = [
      '#type' => 'hidden',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state): void {
    $options = Utils::getPayuConfiguration($this->configFactory->get('payu_donations.settings'));

    if (empty(array_filter($options))) {
      $link = Url::fromRoute('payu_donations.settings_form')->toString();
      $form_state->setErrorByName('payu_options', $this->t('Before adding a block please configure payU global settings <a target="_blank" href="@link_settings">here</a>', ['@link_settings' => $link]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state): void {
    $this->configuration['block_id'] = $form_state->getBuildInfo()['callback_object']->getEntity()->id();

    $configs = [
      'payu_environment',
      'payu_pos_id',
      'payu_second_key_md5',
      'payu_client_id',
      'payu_client_secret',
      'payu_signature_key',
      'payu_currency',
      'payu_payment_description',
      'payu_submit_button_text',
      'payu_succeed_url_redirect',
      'payu_canceled_url_redirect',
    ];

    foreach ($configs as $config) {
      $this->configuration[$config] = $form_state->getValue($config);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    if (!class_exists(\OpenPayU_Configuration::class)) {
      $this->payuLogger->error('You must install OpenPayU library to use PayU Donations module!');

      return [
        '#type' => 'html_tag',
        '#tag' => 'span',
        '#value' => $this->t('You must install OpenPayU library to use PayU Donations module!'),
      ];
    }

    $block_id = $this->configuration['block_id'] ?? NULL;
    $form_options = $this->getPayuFormConfiguration();

    $form = $this->formBuilder->getForm(
      'Drupal\payu_donations\Form\PayUForm',
      $form_options['payu_submit_button_text'],
      $block_id
    );

    return [
      '#theme' => 'payu_donations_block',
      '#form' => $form,
    ];
  }

  /**
   * Returns payU form configuration.
   *
   * @return array
   *   Array with form configuration.
   */
  protected function getPayuFormConfiguration(): array {
    $form_configuration = [
      'payu_submit_button_text' => $this->configuration['payu_submit_button_text'] ?? NULL,
    ];

    if (count(array_filter($form_configuration)) !== 1) {
      $empty_form_configuration = array_filter($form_configuration, fn($configuration) => $configuration == NULL);

      foreach ($empty_form_configuration as $single_config => $value) {
        $form_configuration[$single_config] = $this->configFactory->get('payu_donations.settings')->get($single_config);
      }
    };

    return $form_configuration;
  }

}
